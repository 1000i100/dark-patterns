# Dark Patterns

Affronte le plus redoutable des adversaires : toi-même !

Le jeu t'observe et apprends... Apprendras-tu de lui plus vite qu'il n'apprend de toi ?

Formée aux techniques de manipulation psychologique du neuro-marketing,
l'IA de ce Free2Play essera d'en apprendre le plus possible sur vous affin d'identifier vos faiblesses pour vous influencer le plus efficacement possible.

Vous avez le même objectif : faire le plus gros score... Mais pas dans les mêmes métriques.


Ce free2play est-il un pay2win ?
- pour l'IA du jeu : oui totalement ! Le score qu'elle souhaite faire exploser, c'est vos dépenses dans le jeu (et votre temps passé dessus)
- pour vous : officiellement oui... enfin non. Officieusement non, enfin c'est plus compliqué.
  Il y a l'objectif initial que vous propose le jeu, maximiser votre score quitte à payer pour. Mais aussi l'objectif au quel vous invite le jeu : acquérire un regard critique sur vos comportements compulsif influencé par ce jeu, et idéalement étendre ce regard au reste de votre vie et de ce qui l'influence (publicité, promotions, autres jeux, média sociaux...). Puisqu'il faut se tromper pour apprendre, il vous manquera des choses si vous n'explorez pas les aspects payants, et pour autant, c'est peutêtre que vous savez déjà vous en protéger... (Quand le contexte vous y invite tout du moins). Pour vous permettre d'explorer le rapport à l'argent dans le jeu, sans vider le compte qui vous sers à manger et payer vos impots, vous pourrez utiliser une monnaie alternative (la Ǧ1) au nombreuses vertue que vous pourrez découvrir si vous décidez de l'utiliser. PS : non, cette monnaie n'est pas propre au jeu (pas plus que l'euro), si vous voulez en savoir plus indépendament de Dark-Patterns, rendez-vous sur https://monnaie-libre.fr/ 



## Roadmap - feuille de route

- [x] Décrire le projet en quelques mots
- [ ] Concevoir le gameplay de premier niveau (les mécanisme de jeu manipulé par le joueur à son arrivée dans le jeu). Probablement Clicker game.
- [ ] Concevoir le méta-gameplay, comment confronter le joueur à ces bias cognitifs ? Comment l'exposer aux [pires technique de neuro-marketing](https://fr.wikipedia.org/wiki/Dark_pattern), et comment l'aider à les identifier de manière ludique ? (promo unique, offre flash, récompense aléatoire (avec grosses récompenses visible présenté comme équiprobable alors que non), personnalisation en fonction de l'historique comportemental, concurence (truqué) entre joueurs, implication social via multi-joueur et réseau sociaux, parrainage viral... et technique hybride mi-dark, mi-vertueuse d'implication ethique % charitatif, ciblage choisi par l'usager... pour légitimer l'achat compulsif et le rend plus rationnalisable)
- [ ] Background et narration orienté neuro-marketing pour brouiller les frontières entre gameplay et méta-gameplay tout en améliorant l'immersion.
- [ ] Financer le projet (crowd-funding ?)
- [ ] Coder un prototype
- [ ] Gérer les fonctionnalités de paiement en jeu
- [ ] Améliorer l'existant sur tout les aspects.
- [x] Publier l'ensemble sous licence libre dès le premier jour !


## Ressources

- https://fr.wikipedia.org/wiki/Dark_pattern
- https://fr.slideshare.net/sebw/le-free-to-play-principes-et-lments-de-rflexion-sept12
- https://darkpatterns.org/types-of-dark-pattern.html
- https://simplicable.com/new/dark-patterns
- https://designshack.net/articles/ux-design/dark-patterns/
- https://boagworld.com/marketing/dark-patterns/
- https://www.researchgate.net/publication/226228201_Neuromarketing_The_New_Science_of_Consumer_Behavior


